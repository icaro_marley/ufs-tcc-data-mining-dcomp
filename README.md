﻿# UFS-TCC: Data Mining DCOMP
Trabalho de Conclusão de Curso de Ciência da Computação, onde foram analisados os dados de estudantes do Departamento de Computação da Universidade Federal de Sergipe

# Tecnologias utilizadas
- Pandas e Numpy (Manipulação de dados)
- Matplotlib (Visualização de dados)
- Sklearn (Machine Learning)
- FIM (Regras de Associação, http://www.borgelt.net/pyfim.html)