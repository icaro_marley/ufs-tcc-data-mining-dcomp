# -*- coding: utf-8 -*-
"""
Created on Mon Aug 14 20:35:11 2017

@author: icaromarley55

pergunta:
    
quais são os perfis dos alunos?

"""

# imports
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
from sklearn.preprocessing import StandardScaler
from sklearn import decomposition
from mpl_toolkits.mplot3d import axes3d 
import seaborn as sns


pd.options.display.float_format = '{:,.2f}'.format

data_path = '../data/modified data/DadosBancoCSV/'

df_students_semesters = pd.read_csv(data_path+'tabela_estudante_semestre.csv',encoding="latin-1",sep=';',low_memory=False)
    
# últimos dados dos estudantes
df_students_last_semester = df_students_semesters.groupby(['dmal_id_aluno']).last()
   
# selecionando variaveis
columns = [
 'ftda_vl_mgp',
 'ftda_vl_ir',
 'n_períodos',
 ]

df_students_last_semester['desempenho_aluno'].value_counts(dropna=False)
'''
descartando 272 alunos cujo valor de desempenho_aluno é nulo
'''
X = df_students_last_semester[columns].dropna()
X = StandardScaler().fit_transform(X)

# clustering com DBSCAN
db = DBSCAN(eps=.5).fit(X)
labels = db.labels_
# avaliar
# 3d plot
fig = plt.figure(figsize=(10,5))
ax1 = fig.add_subplot(1, 1, 1, projection='3d')
x = X[:,0]
y = X[:,1]
z = X[:,2]
k = labels
for label in np.unique(k):
    name = 'Grupo ' + str(label)
    if label == -1:
        name = 'Outlier'
    ax1.scatter(x[np.where(labels==label)], 
        y[np.where(labels==label)],
        z[np.where(labels==label)],
        label=name)
plt.legend()
plt.title('Agrupamentos dos estudantes conforme métricas acadêmicas')
ax1.set_xlabel('Variável 1')
ax1.set_ylabel('Variável 2')
ax1.set_zlabel('Variável 3')
plt.show()

'''
É possível perceber que há apenas 2 agrupamentos e alguns outliers
'''
df_students_last_semester['grupo'] = labels
df_students_last_semester['grupo'].value_counts()
'''
tamanho dos grupos:
 0    1630
 1     354
-1       4
'''
df_students_last_semester.groupby('grupo')['estado'].value_counts()
# os 4 outliers abandonaram os cursos
explain_columns = [
 'estado',
 'dmal_ds_sexo',
 'dmal_ds_cota',
 'dmal_ds_forma_de_ingresso',
 'dmal_ds_tipo_necessidade_especial',
 'dmal_ds_periodo_de_ingresso',        
 'dmcs_nm_curso',       
 'n_escolaridade',
 'dmal_dt_nascimento',   
 'ftda_vl_mgp',
 'ftda_vl_ir', 
 'n_períodos',
] 

df_students_last_semester.loc[df_students_last_semester['grupo'] == 1,explain_columns].describe()
df_students_last_semester.loc[df_students_last_semester['grupo'] == 0,explain_columns].describe()

'''
O grupo 0 possui alunos com entrada mais antiga (2010), a idade é similar ao outro grupo, 
junto com o nível de escolaridade (escola particular/pública)
a mgp é acima de 5 e ir é proxima de 0.5
estão em torno do 7 período.

o grupo 1 possui mgp e ir proximas ou igual a zero e estão em torno do segundo período
São alunos jovens e de entrada recente (2012). O nível de escolaridade é proximo ao 2 (escola particular/pública)
'''
# desconsiderando outliers
df_students_last_semester = df_students_last_semester[df_students_last_semester['grupo']!=-1]
plot_columns = [
 'estado',
 'dmal_ds_sexo',
 'dmal_ds_cota',
 'dmal_ds_forma_de_ingresso',
 'dmal_ds_tipo_necessidade_especial',
 'dmal_ds_periodo_de_ingresso',        
 'dmcs_nm_curso',       
 'n_escolaridade',
 'dmal_dt_nascimento',   
 'n_períodos',
] 

for column in plot_columns:
    data = df_students_last_semester.groupby('grupo')[column].value_counts().rename('valores').reset_index()
    sns.barplot('valores',column,orient='h',hue='grupo',data=data)
    plt.title(column)
    plt.show() 
    
'''
1 É composto em sua maior parte por 
alunos que abandonaram os cursos

0 É composto em sua maior parte por
alunos regulares  ou que se formaram (em detrimento de abandono ou irregularidade)
alunos de escola particular/pública
'''