# -*- coding: utf-8 -*-

"""
6- Qual será o valor da próxima MGP de um aluno do departamento?
"""

import pandas as pd
from sklearn.metrics import mean_absolute_error
from model_tools import train_summarize

'''
df_students_last_semester['dmcs_nm_curso'].value_counts()
Ciência Da Computação        1066
Sistemas de Informação        492
Engenharia de Computação      430
como existem poucos alunos nos outros dois cursos, vou considerar o grupo inteiro de dados 
e treinar um modelo só
'''

data_path = '../data/modified data/DadosBancoCSV/'

df_students_semesters = pd.read_csv(data_path+'tabela_estudante_semestre.csv',encoding="latin-1",sep=';',low_memory=False)

# ultimos dados dos estudantes
df_students_last_semester = df_students_semesters.groupby(['dmal_id_aluno']).last()
    
def negative_mean_absolute_error(model, X_pred, y_true,):
    return -mean_absolute_error(y_true,model.predict(X_pred))

target = 'ftda_vl_mgp'

# selecao de colunas

'''
seleção de variaveis
informações do aluno
métricas do aluno no semestre atual (início do semestre)
métricas do aluno no semestre anterior
'''
import numpy as np
df_students_last_semester['teste'] = np.NaN
X_columns = [
 # informações do aluno
 'dmcs_nm_curso',
 'n_escolaridade',
 'dmal_dt_nascimento',
 'dmal_ds_sexo',
 'dmal_ds_forma_de_ingresso',
 'dmal_ds_tipo_necessidade_especial',
 'n_semestre_entrada',

 # informações do semestre
 'quantidade_professores_notas_baixas', 
 'quantidade_matérias_notas_baixas',
 'semestres_atraso_semestre',
 'ftda_qt_credito_oficial',
 'creditos_atrasados',
 'n_períodos',

 # métricas do aluno até o semestre
 'semestres_atraso_media',
 'ftda_qt_credito_oficial_media',
 'ftda_vl_mgp_anterior',
 'ftda_vl_ir_anterior',
 'ftda_qt_credito_real_anterior',
 'desempenho_aluno_anterior',
]

train_summarize(df_students_last_semester, X_columns, target, 'regression', negative_mean_absolute_error, 'q6')




print('Fim do treinamento. Checar arquivos gerados')

'''
As avaliações são feitas em torno das previsões do modelo considerando os valores (apenas) da variável em questão.

Pontuação 0.16 de erro médio com desvio padrão de 0.03
Existe uma diferença (em torno de 0.1) entre teste e treinamento, que pode indicar um leve nível de overfitting.
Mgp anterior possui a maior contribuição para o modelo. Depois, créditos atrasados.
As outras variáveis não tem grande influência.
mgp anterior tem alta influencia nas decisões do modelo, com valores mais altos da mgp anterior indicando uma próxima mgp também mais alta
créditos atrasados acima de 25 influenciam a mgp, embora faltem dados para valores acima de 30
'''