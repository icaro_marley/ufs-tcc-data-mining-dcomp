# -*- coding: utf-8 -*-
'''
analisa os dois datasets
estudante turma
estudante semestre
'''





'''
df_students_classes[df_students_classes['fttu_qt_credito_real'] == 0]['fttu_qt_aprovado'].value_counts()
# alguns creditos reais zerados e aprovados
# quem sao essas disciplians
df_students_classes[(df_students_classes['fttu_qt_credito_real'] == 0) & (df_students_classes['fttu_qt_aprovado'] == 1)]['dmcc_id_componente_curricular'].unique()

# todas as aprovadas com cred real zero tinham cred oficial 0
# dropar? melhor não.
df_students_classes[(df_students_classes['fttu_qt_credito_real'] == 0) & (df_students_classes['fttu_qt_aprovado'] == 1)]['fttu_qt_credito_oficial'].value_counts()
'''

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np

data_path = '../data/modified data/DadosBancoCSV/'

def plot_to_bar(value_counts,title):
    # bar plot ir
    ax = value_counts.plot(figsize=(50,10),kind='bar', legend= False, rot=0, title=title)
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_visible(False)
    ax.spines['bottom'].set_visible(False)
    ax.xaxis.label.set_visible(False)
    y_axis = ax.axes.get_yaxis()
    y_axis.set_visible(False)
    for p in ax.patches:
        ax.annotate((int(p.get_height())), (p.get_x() + p.get_width() / 2, p.get_height()), ha='center', va='center', xytext=(0, 10), textcoords='offset points')
    plt.xticks(rotation=45)
    plt.show()


pd.options.display.float_format = '{:,.2f}'.format
# ESTUDANTE X SEMESTRE
df_students_semesters = pd.read_csv(data_path+'tabela_estudante_semestre.csv',encoding="latin-1",sep=';',low_memory=False)
    

df_students_semesters['dmcs_nm_curso'] = df_students_semesters['dmcs_nm_curso']\
    .replace({"Ciência Da Computação ":"Ciência da Computação",
    "Engenharia de Computação ":"Engenharia da Computaçao"})


df_students_semesters.shape
# 12021 linhas

#nao ha valoresn nulos
df_students_semesters.isnull().any().any()

# checar os ultimos dados dos estudantes
df_students_last_semester = df_students_semesters.groupby(['dmal_id_aluno']).last()

# sexo por curso
data = df_students_last_semester.groupby('dmcs_nm_curso')['dmal_ds_sexo'].\
    value_counts(normalize=True).rename('freq').reset_index()
sns.barplot(x='dmal_ds_sexo',y='freq',hue='dmcs_nm_curso',data=data)
plt.xlabel('')
plt.ylabel('Porcentagem de alunos')
plt.legend().set_title("")
plt.show()

# performance do aluno por curso
data = df_students_last_semester.groupby('dmcs_nm_curso')['desempenho_aluno'].\
    value_counts(normalize=True).rename('freq').reset_index()
sns.barplot(x='desempenho_aluno',y='freq',hue='dmcs_nm_curso',data=data)
plt.xlabel('')
plt.ylabel('Porcentagem de alunos')
plt.legend().set_title("")
plt.show()


# abandono por curso
data = df_students_last_semester.copy()
#data.loc[data['estado'] != "abandonou",'estado'] = False
#data.loc[data['estado'] == "abandonou",'estado'] = True
data = data.groupby('dmcs_nm_curso')['estado'].\
    value_counts(normalize=True).rename('freq').reset_index()\
    .replace({'regular':"Regular",'abandonou':"Abandonou",'irregular':"Irregular"})
plt.figure(figsize=(6,4))
sns.barplot(x='estado',y='freq',hue='dmcs_nm_curso',data=data)
plt.ylabel("Porcentagem de alunos")
plt.xlabel('')
leg = plt.legend(bbox_to_anchor=(1, 1))
leg.set_title("")
plt.show()

# cota
df_students_last_semester['cota_capitalizada'] = df_students_last_semester['dmal_ds_cota'].str.capitalize()
data = df_students_last_semester.groupby('dmcs_nm_curso')['cota_capitalizada'].\
    value_counts(normalize=True).rename('freq').reset_index()
sns.barplot(x='cota_capitalizada',y='freq',hue='dmcs_nm_curso',data=data)
plt.ylabel("Porcentagem de alunos")
plt.xlabel('')
leg = plt.legend(bbox_to_anchor=(1, 1))
leg.set_title("")
leg.get_frame().set_alpha(0.1)
plt.show()
del df_students_last_semester['cota_capitalizada']

# número de períodos
data = df_students_last_semester.groupby('dmcs_nm_curso')['n_períodos'].value_counts()\
    .rename('freq').reset_index().sort_values('n_períodos')
sns.barplot(x='n_períodos',y='freq',hue='dmcs_nm_curso',data=data)
plt.ylabel("Quantidade de alunos")
plt.xlabel('Número de períodos cursados')
plt.legend().set_title("")
plt.show()


n_bins = 10
data = df_students_last_semester['dmal_dt_nascimento']
hist_range = (data.max() - data.min()) / n_bins
ticks_pos = np.arange(data.min(),data.max() + 1,hist_range)
ticks_name = np.vectorize(np.int)(ticks_pos)
ax = data.plot(kind='hist',bins=n_bins)
plt.xticks(ticks_pos,ticks_name,rotation=45)
plt.xlabel("Ano")
plt.ylabel("")
plt.show()

df_students_last_semester['dmal_dt_nascimento'].hist()

df_students_last_semester['dmal_ds_sexo'].value_counts()
df_students_last_semester['abandonou'].value_counts()
df_students_last_semester['dmcs_nm_curso'].value_counts()

df_students_last_semester['idade_aproximada'].hist(bins=50)
plt.xlabel('Idade aproximada')


df_students_last_semester['ftda_vl_mgp'].plot(kind='hist',bins=50)
plt.ylabel("")
plt.xlabel('MGP')


n_bins = 10
data = df_students_last_semester['ftda_vl_mgp']
hist_range = (data.max() - data.min()) / n_bins
ticks_pos = np.arange(data.min(),data.max() + 1,hist_range)
ax = data.plot(kind='hist',bins=n_bins)
plt.xticks(ticks_pos,ticks_name)
plt.xlabel("MGP")
plt.ylabel("")
plt.show()



df_students_last_semester['n_períodos'].hist(bins=50)
plt.xlabel('Número de períodos')
plt.show()


df_students_last_semester['dmal_ds_cota'].value_counts()



ax = sns.regplot(scatter_kws={"s": 5},x='ftda_vl_mgp_anterior',y='ftda_vl_mgp',data=df_students_last_semester,fit_reg=False)
plt.xlabel('MGP anterior')
plt.ylabel('MGP')


df_students_last_semester['dmtm_cd_tempo'].min()
df_students_last_semester['dmtm_cd_tempo'].max()


# segundo df
df_students_classes = pd.read_csv(data_path+'tabela_estudante_turma.csv',encoding="latin-1",sep=';',low_memory=False)
    
df_students_classes['dmcs_nm_curso'] = df_students_classes['dmcs_nm_curso']\
    .replace({"Ciência Da Computação ":"Ciência da Computação",
    "Engenharia de Computação ":"Engenharia da Computaçao"})


len(df_students_classes['dmcc_nm_componente_curricular'].unique())
len(df_students_classes['dmal_id_aluno'].unique())

# aprovados reprovados e trnacados por curso
columns = ['fttu_qt_reprovado','fttu_qt_aprovado','fttu_qt_trancado']

data = df_students_classes.groupby("dmcs_nm_curso")[columns]\
    .sum().reset_index()
len_data = df_students_classes["dmcs_nm_curso"].value_counts().rename('len').reset_index()
data = data.merge(len_data, left_on='dmcs_nm_curso',right_on='index')    
data[columns] = data[columns].divide(data['len'].values,axis=0)
data = pd.melt(data, id_vars=['dmcs_nm_curso'],value_vars=columns,
                  var_name="Tipo", value_name="Quantidade")
ax = sns.barplot(x='Tipo',y='Quantidade',hue='dmcs_nm_curso',data=data)
plt.ylabel("Porcentagens")
plt.xlabel('')
ax.set_xticklabels(["Reprovações","Aprovações","Trancamentos"])
leg = plt.legend(bbox_to_anchor=(1, 1))
leg.set_title("")
leg.get_frame().set_alpha(0.1)
plt.show()

'''
dmal_dt_nascimento media ok baixa std
dmal_ds_periodo_de_ingresso min 1992 max 2015
dmtm_cd_tempo min 20071 max  20154. checar contra dmal_ds_periodo_de_ingresso
crédito_aluno mean ok, std alta, max 152, min zero. checar max
ftda_vl_ir min 0, std alta, max 1.21
dmtm_nr_ano mesmo q dmtm_cd_tempo
dmtm_nr_semestre ok
n_reprovadas max 57, mean ok, std 
n_trancadas mean ok, std baixa, max 19
n_aprovadas max 62, mean ok, std alta
n_perdidas mean ok, std medio, max 60
n_períodos max 20, rechecar variavel, std alto, mean ok
total_créditos mean ok, std alta, min 2
média_créditos mean ok, std alta,
idade_aproximada mean ok, std baixa, max 70, min 16
n_atrasos mean alta, std alta
n_adiantamentos mean ok, std alto, max 110
'''

# comparacao entre dmtm_cd_tempo e dmal_ds_periodo_de_ingresso
checar = df_students_semesters.groupby(['dmal_id_aluno']).first()[['dmtm_cd_tempo','dmal_ds_periodo_de_ingresso']]
checar['dmtm_cd_tempo'] = checar['dmtm_cd_tempo']/10
checar = checar[checar['dmal_ds_periodo_de_ingresso'] != checar['dmtm_cd_tempo']]
# alguns menores e maiores que o outro [checar['dmal_ds_periodo_de_ingresso'] > checar['dmtm_cd_tempo']
#presença de 'dmtm_nr_semestre' em tempo. não bate com fttu_nr_periodo quando unido pelo codigo do tempo dmtm_cd_tempo
# nr semestre é o semestre onde a disciplina foi dada
# existem alunos que pegam mais de 100 créditos.
checar = df_students_semesters[df_students_semesters['crédito_aluno'] > 100]

#hists
df_students_last_semester['dmal_dt_nascimento'].plot(kind='hist',bins=50)
df_students_last_semester['n_reprovadas'].plot(kind='hist',bins=50)
df_students_last_semester['n_trancadas'].plot(kind='hist',bins=50)
df_students_last_semester['crédito_aluno'].plot(kind='hist',bins=50)
df_students_last_semester['ftda_vl_mgp'].plot(kind='hist',bins=50)
df_students_last_semester['ftda_vl_ir'].plot(kind='hist',bins=50)
df_students_last_semester['n_aprovadas'].plot(kind='hist',bins=50)
df_students_last_semester['n_perdidas'].plot(kind='hist',bins=50)
df_students_last_semester['n_períodos'].plot(kind='hist',bins=50)
df_students_last_semester['total_créditos'].plot(kind='hist',bins=50)
df_students_last_semester['média_créditos'].plot(kind='hist',bins=50)
df_students_last_semester['idade_aproximada'].plot(kind='hist',bins=50)
df_students_last_semester['dmtm_nr_ano'].plot(kind='hist',bins=50)
df_students_last_semester['n_atrasos'].plot(kind='hist',bins=50)
df_students_last_semester['n_adiantamentos'].plot(kind='hist',bins=50)

'''
n_reprovadas,n_trancadas, n_aprovadas, ftda_vl_ir, n_perdidas  skew ->
um dos motivos para n_aprovadas estar com skew -> e  n_perdidas -> pode ser um grande abandono do curso e estudantes recentes
n_periodos reforça isso, com uma maior quantidade de alunos nos primeiros periodos
dmtm_nr_ano mostra um grande numero de estudantes recentes
346 alunos com ir 0 e mgp 0
total creditos burst de creditos totais em 28. deve ser o pessoal do primeiro semestre.
idade media jovem
moda de creditos em torno de 30. ok
existem pessoas com muitos adiantamento
'''

# quem tem ir 0 tem mgp  0
(df_students_last_semester[df_students_last_semester['ftda_vl_ir'] ==0]\
    ['ftda_vl_mgp'] == 0).all()

#bar plot dos cursos dmcs_id_curso
plot_to_bar(df_students_last_semester['dmal_ds_sexo'].value_counts(),
    "Sexo dos Estudantes")
plot_to_bar(df_students_last_semester['dmal_ds_forma_de_ingresso'].value_counts(),
    "Forma de ingresso dos Estudantes")
plot_to_bar(df_students_last_semester['dmal_ds_cota'].value_counts(),
    "Cota dos Estudantes")
plot_to_bar(df_students_last_semester['dmcs_nm_curso'].value_counts(),
    "Curso dos Estudantes")
plot_to_bar(df_students_last_semester['dmal_ds_tipo_necessidade_especial'].value_counts(),
    "Necessidade especial dos Estudantes")
plot_to_bar(df_students_last_semester['dmtm_cd_tempo'].value_counts(),
    "Último registro  dos Estudantes")

'''
1735 homens, 253 mulheres
1756 entraram via vestibular
maior parte é sem cota(525)
1066 alunos de CC
859 dos alunos esta em 2015.2
'''


'''
alunos do 'primeiro periodo' pegando grafos.
equivalencia é uma possibilidade.
'''

df_students_last_semester.columns

df_students_classes.columns

df_students_classes.shape
# 50984 linhas, 

#nao ha valoresn nulos
df_students_classes.isnull().any().any()


df_students_classes.describe()
'''
dmtm_cd_tempo ok
crédito_disciplina min 0
'''
checar = df_students_classes[df_students_classes['crédito_disciplina']==0]
checar = checar[['dmcc_nm_componente_curricular','dmcc_tp_componente_curricular']].drop_duplicates()
#restringir por credito, nome

# dropar atividades e fazer recalculo de creditos?
'''
pode atrapalhar ainda mais a quantidade de creditos

'''


#hists
df_students_classes['fttu_vl_nota'].plot(kind='hist',bins=50)
'''
quase 13 mil notas zero
'''

#bar plot dos cursos dmcs_id_curso
plot_to_bar(df_students_classes['dmtm_nr_ano'].value_counts(),
    "Ano das disciplinas")
plot_to_bar(df_students_classes['curso_disciplina'].value_counts(),
    "Curso das disciplinas das turmas")
print(df_students_classes['dmpr_nm_professor'].value_counts())
plot_to_bar(df_students_classes['fttu_qt_trancado'].value_counts(),
    "Quantidade de disciplinas trancadas")
plot_to_bar(df_students_classes['fttu_qt_aprovado'].value_counts(),
    "Quantidade de alunos aprovados")
plot_to_bar(df_students_classes['fttu_qt_reprovado'].value_counts(),
    "Quantidade de alunos reprovados")
plot_to_bar(df_students_classes['crédito_disciplina'].value_counts(),
    "Crédito das disciplinas")
plot_to_bar(df_students_classes['fttu_nr_periodo'].value_counts(),
    "Período oficial das disciplinas")
print(df_students_classes['dmcc_nm_componente_curricular'].value_counts())
plot_to_bar(df_students_classes['dmcc_tp_componente_curricular'].value_counts(),
    "Tipo das disciplinas das turmas")
plot_to_bar(df_students_classes['semestres_adiantamento'].value_counts(),
    "Quantidade de semestres adiantados dos alunos")
plot_to_bar(df_students_classes['semestres_atraso'].value_counts(),
    "Quantidade de semestres atrasados dos alunos")

'''
maior parte estao no primeiro e no segundo periodo
maior parte em 2010 e diante
maior parte é de cc
2831 turmas sem professor identificado
47202 foram trancadas
27774 alunos aprovados
19428 reprovados
28884 turmas de disciplinas de 4 creditos
maior parte com 0 semestres atrasados, alguns com 1 ou 2
maior parte com 0 semestres adiantados
maior parte dos alunos (+2000 cada) está em C1, VGA, PI, seguindos de POO com 1842
50352 são disciplinas,  632 são atividades
'''