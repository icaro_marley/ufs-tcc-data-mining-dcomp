# -*- coding: utf-8 -*-

'''
Módulo voltado para o pré-processamento dos arquivos .csv para gerar 
2 tabelas que serão utilizadas no resto do projeto:
>aluno x semestre, que guarda o desempenho dos alunos ao longo dos semestres
>aluno x turma, que guarda o desempenho dos alunos em cada turma

lista de arquivos utilizados:
public_dmal_aluno.csv
public_dmcs_curso.csv
public_dmpr_professor.csv
public_dmtm_tempo.csv
public_dmcc_componente_curricular.csv
public_turma.csv
public_fttu_desempenho_turma.csv

arquivos gerados:
tabela_estudante_semestre.csv
tabela_estudante_turma.csv
    
'''

import pandas as pd
import re
import numpy as np

# transformações básicas

def code_to_int(string):
    string = str(string)
    return int(re.sub(r"^$","0",string.replace(",000000","")))

def str_to_float(string):
    string = str(string)
    return float(string.replace(",","."))

def date_to_year(string):
    string = str(string)
    return int(re.sub("[0-9]{2}\/[0-9]{2}\/","",string))

data_path = '../data/modified data/DadosBancoCSV/'


'''
public_dmal_aluno.csv
informações sobre os alunos
'''
file_name = 'public_dmal_aluno.csv'
df_students = pd.read_csv(data_path+file_name,encoding="latin-1",sep=';',low_memory=False)
df_students['dmal_dt_nascimento'] = df_students['dmal_dt_nascimento'].apply(date_to_year)

# consertando tipagem e substituindo nulos
df_students['dmal_id_aluno'] = df_students['dmal_id_aluno'].apply(code_to_int)
# valores nan -> Nenhuma
df_students['dmal_ds_tipo_necessidade_especial'] = \
    df_students['dmal_ds_tipo_necessidade_especial']\
    .astype(str).replace({"nan":"Nenhuma"})
# modificando codificação
df_students['dmal_nr_periodo_concluso'] = df_students['dmal_nr_periodo_concluso']\
    .replace({'null':'0'}).replace({np.nan:'0'}).apply(code_to_int)    
df_students['dmal_qt_total_periodo'] = df_students['dmal_qt_total_periodo']\
    .replace({'null':'0'}).replace({np.nan:'0'}).apply(code_to_int)
df_students['dmal_in_concluiu'] = df_students['dmal_in_concluiu'].apply(code_to_int)

'''
Nível de escolaridade:
superior incompleto 3
ensino médio 2 
ensino médio em escola pública 1
http://sisu.ufs.br/uploads/page_attach/path/3562/EDITAL_01_-_SISU_2018_-_RETIFICADO.pdf
'''
def compute_scholarity(row):
    if row['dmal_ds_forma_de_ingresso'] not in ['Vestibular','Convênio','Sub Judice']:
        return 3
    if row['dmal_ds_cota'] not in ['A','AC','PD',np.nan]:
        return 1
    return 2
df_students['n_escolaridade'] = df_students.apply(compute_scholarity,axis=1)
    
'''
Cota
nans antes de 2007: sem sistema de cotas
nans depois de 2007: sem cota
A e AC: sem cota
resto: cota
todos que nao entraram em Vestibular ou Readmissão sao sem cota.
'''
membership_year = 2010
membership_admission = ['Vestibular','Readmissão']
def membership_transform(row):
    membership = str(row['dmal_ds_cota'])
    year = row['dmal_ds_periodo_de_ingresso']
    admission = row['dmal_ds_forma_de_ingresso']
    if admission not in membership_admission:
        return "sem cota"
    if membership in ["AC","A"]:
        return "sem cota"
    if membership == 'nan':
        if year >= membership_year:
            return "sem cota"
        if year < membership_year:
            return "sem sistema de cotas"
    return "com cota"
df_students['dmal_ds_cota'] = \
    df_students\
    .apply(membership_transform,axis=1)

# seleção
columns = [
    'n_escolaridade',
    'dmal_dt_nascimento',
    'dmal_id_aluno',
    'dmal_ds_sexo',
    'dmal_ds_periodo_de_ingresso',
    'dmal_ds_forma_de_ingresso',
    'dmal_ds_tipo_necessidade_especial',
    'dmal_ds_cota',
    'dmal_nr_periodo_concluso',
    'dmal_qt_total_periodo',
    'dmal_in_concluiu']
df_students = df_students[columns]

'''
public_dmcs_curso.csv
informações sobre os cursos
'''
file_name = 'public_dmcs_curso.csv'
df_courses = pd.read_csv(data_path+file_name,encoding="latin-1",sep=';',low_memory=False)

# modificando tipagem e valores 
df_courses['dmcs_id_curso'] = df_courses['dmcs_id_curso'].apply(code_to_int)
# removendo São Cristóvão para facilitar leitura
df_courses['dmcs_nm_curso'] = df_courses['dmcs_nm_curso'].\
    apply(lambda string: re.sub("Bacharelado\/.*","",string))
    
# seleção
columns = [
    'dmcs_id_curso',
    'dmcs_nm_curso',]
df_courses = df_courses[columns]


'''
public_dmpr_professor.csv
informações sobre os professores 
'''
file_name = 'public_dmpr_professor.csv'
df_teachers = pd.read_csv(data_path+file_name,encoding="latin-1",sep=';',low_memory=False)

# consertando tipos e valores
df_teachers['dmpr_id_professor'] = df_teachers['dmpr_id_professor'].apply(code_to_int)
df_teachers['dmpr_nm_professor'] = df_teachers['dmpr_nm_professor'].replace({'Não identificado':np.NaN})
map_dict = {
    'PROFESSOR 3 GRAU - SUBSTITUTO':'substituto',
    'PROFESSOR DO MAGISTERIO SUPERIOR':'casa',
    'PROFESSOR ENS BASICO TECNICO TECNOLOGICO':'casa',
    'PROFESSOR 3 GRAU':'casa',
    'PROFESSOR TEMPORARIO':'substituto',
    'PROFESSOR DO MAGISTERIO SUPERIOR-SUBSTITUTO':'substituto',
    'PROFESSOR 3 GRAU - VISITANTE':'substituto',
    'PROF ENS BAS TEC TECNOLOGICO-SUBSTITUTO':'substituto',    
}
# preenchendo tipo do professor
df_teachers['dmpr_tp_professor'] = df_teachers['dmpr_tp_professor'].replace(map_dict)
def compute_teacher_type(row):
    value = row['dmpr_tp_professor']
    if value == 'N/I' and  row['dmpr_ds_formacao'] in\
        ['SEM TITULAÇÃO', 'MESTRADO', 'GRADUAÇÃO', 'ESPECIALIZAÇÃO', 'APERFEIÇOAMENTO']:
        return 'substituto'
    return value
df_teachers['dmpr_ds_formacao'] = df_teachers.apply(compute_teacher_type,axis=1).value_counts()

# seleção
columns = [
    'dmpr_id_professor',
    'dmpr_nm_professor',
    'dmpr_tp_professor',]
df_teachers = df_teachers[columns]

'''
public_dmtm_tempo.csv
informações sobre os semestres
'''
file_name = 'public_dmtm_tempo.csv'
df_semesters = pd.read_csv(data_path+file_name,encoding="latin-1",sep=';',low_memory=False)

# consertando tipos
df_semesters['dmtm_cd_tempo'] = df_semesters['dmtm_cd_tempo'].apply(code_to_int) 
df_semesters['dmtm_nr_ano'] = df_semesters['dmtm_nr_ano'].apply(code_to_int)
# seleção
columns = [
    'dmtm_cd_tempo',
    'dmtm_nr_ano']
df_semesters = df_semesters[columns]

# criando estrutura de dados que marca a posição dos semestres
def remove_semesters(semester):
    if (str(semester)[-1] == "1") or (str(semester)[-1] == "2"):
        return semester
semester_numbers = df_semesters['dmtm_cd_tempo'].apply(remove_semesters).dropna().sort_values() / 10
semester_numbers.index = np.arange(1,semester_numbers.shape[0]+1)

'''
public_dmcc_componente_curricular.csv
informações sobre as disciplinas
'''
file_name = 'public_dmcc_componente_curricular.csv'
df_subjects = pd.read_csv(data_path+file_name,encoding="latin-1",sep=';',low_memory=False)

# consertando tipagem
df_subjects['dmcc_id_componente_curricular'] = df_subjects['dmcc_id_componente_curricular']\
    .apply(code_to_int)

# seleção
columns = [
    'dmcc_id_componente_curricular',
    'dmcc_nm_componente_curricular',
    'dmcc_tp_componente_curricular',      
    ]
df_subjects = df_subjects[columns]

'''
public_ftda_desempenho_aluno.csv 
informações sobre o desempenho do aluno ao longo dos semestres
'''
file_name = 'public_ftda_desempenho_aluno.csv'
df_students_performances = pd.read_csv(data_path+file_name,encoding="latin-1",sep=';',low_memory=False)

# consertando tipagem
df_students_performances['ftda_nr_media_credito_curso'] = df_students_performances[
    'ftda_nr_media_credito_curso'].str.replace(',','.').astype(float)
df_students_performances['ftda_nr_credito_curso'] = df_students_performances['ftda_nr_credito_curso']\
    .apply(code_to_int)
df_students_performances['dmtm_cd_tempo'] = df_students_performances['dmtm_cd_tempo']\
    .apply(code_to_int)
df_students_performances['dmal_id_aluno'] = df_students_performances['dmal_id_aluno']\
    .apply(code_to_int)
df_students_performances['ftda_qt_credito_oficial'] = df_students_performances['ftda_qt_credito_oficial']\
    .apply(code_to_int)
df_students_performances['ftda_qt_credito_real'] = df_students_performances['ftda_qt_credito_real']\
    .apply(code_to_int)
df_students_performances['ftda_vl_mgp'] = df_students_performances['ftda_vl_mgp']\
    .apply(str_to_float)
df_students_performances['ftda_vl_ir'] = df_students_performances['ftda_vl_ir']\
    .apply(str_to_float)
df_students_performances['ftda_ultima_mgp'] = df_students_performances['ftda_ultima_mgp']\
    .apply(str_to_float)
df_students_performances['ftda_ultima_ir'] = df_students_performances['ftda_ultima_ir']\
    .apply(str_to_float)
df_students_performances['dmcs_id_curso'] = df_students_performances['dmcs_id_curso']\
    .apply(code_to_int)
df_students_performances['ftda_qt_formado'] = df_students_performances['ftda_qt_formado']\
    .apply(code_to_int)    
# seleção
columns = [
    'dmtm_cd_tempo',
    'dmal_id_aluno',
    'ftda_qt_credito_oficial',      
    'ftda_vl_mgp',
    'ftda_vl_ir',
    'dmcs_id_curso',
    'ftda_ultima_mgp',
    'ftda_ultima_ir',
    'ftda_qt_credito_real',
    'ftda_nr_credito_curso',
    'ftda_nr_media_credito_curso',
    'ftda_qt_formado',
    ]
df_students_performances = df_students_performances[columns]

'''
public_turma.csv
informações sobre as turmas
'''
file_name = 'public_turma.csv'
df_classes = pd.read_csv(data_path+file_name,encoding="latin-1",sep=';',low_memory=False)

# consertando tipagem
df_classes['id_turma'] = df_classes['id_turma']\
    .apply(code_to_int)
df_classes['dmcc_id_componente_curricular'] = df_classes['dmcc_id_componente_curricular']\
    .apply(code_to_int)
df_classes['dmpr_id_professor'] = df_classes['dmpr_id_professor']\
    .apply(code_to_int)    
df_classes['dmtm_cd_tempo'] = df_classes['dmtm_cd_tempo']\
    .apply(code_to_int)       
        

# seleção
columns = [
    'id_turma',
    'dmcc_id_componente_curricular',
    'dmpr_id_professor',     
    'dmtm_cd_tempo',
    ]
df_classes = df_classes[columns]

'''
public_fttu_desempenho_turma.csv
informações sobre o desempenho dos alunos em cada turma
'''
file_name = 'public_fttu_desempenho_turma.csv'
df_classes_performances = pd.read_csv(data_path+file_name,encoding="latin-1",sep=';',low_memory=False)

# consertando tipagem
df_classes_performances['dmal_id_aluno'] = df_classes_performances['dmal_id_aluno']\
    .apply(code_to_int)
df_classes_performances['dmpr_id_professor'] = df_classes_performances['dmpr_id_professor']\
    .apply(code_to_int)
df_classes_performances['dmtm_cd_tempo'] = df_classes_performances['dmtm_cd_tempo']\
    .apply(code_to_int)
df_classes_performances['fttu_nr_periodo'] = df_classes_performances['fttu_nr_periodo']\
    .apply(code_to_int)    
df_classes_performances['fttu_qt_credito_oficial'] = df_classes_performances['fttu_qt_credito_oficial']\
    .apply(code_to_int)   
df_classes_performances['fttu_qt_credito_real'] = df_classes_performances['fttu_qt_credito_real']\
    .apply(code_to_int)   
df_classes_performances['fttu_qt_aprovado'] = df_classes_performances['fttu_qt_aprovado']\
    .apply(code_to_int)     
df_classes_performances['fttu_qt_reprovado'] = df_classes_performances['fttu_qt_reprovado']\
    .apply(code_to_int)     
df_classes_performances['fttu_qt_trancado'] = df_classes_performances['fttu_qt_trancado']\
    .apply(code_to_int)   
df_classes_performances['dmcc_id_componente_curricular'] = df_classes_performances['dmcc_id_componente_curricular']\
    .apply(code_to_int)  
df_classes_performances['id_turma'] = df_classes_performances['id_turma']\
    .apply(code_to_int)      
'''
algumas notas nulas, são alunos que reprovaram ou trancaram. setando null para zero
df_classes_performances[(df_classes_performances['fttu_vl_nota'] == 'null')]['fttu_qt_aprovado']\
    .astype(str).value_counts()
'''
df_classes_performances['fttu_vl_nota'].replace({"null":"0"},inplace=True)
df_classes_performances['fttu_vl_nota'] = df_classes_performances['fttu_vl_nota']\
    .apply(str_to_float)   
# seleção
columns = [
       'id_turma',
       'dmcc_id_componente_curricular',
       'dmal_id_aluno', 
       'dmpr_id_professor', 
       'dmtm_cd_tempo',
       'fttu_nr_periodo',
       'fttu_qt_credito_oficial', 
       'fttu_qt_aprovado',
       'fttu_qt_reprovado', 
       'fttu_qt_trancado', 
       'fttu_vl_nota',
       'fttu_qt_credito_real',
    ]
df_classes_performances = df_classes_performances[columns]
'''
alunos possuem múltiplas entradas em uma disciplina em um mesmo semestre
(porém com professores e turmas diferentes)
vou considerar apenas o primeiro registro
'''
def remove_duplicates(values):
    return values.iloc[0]
df_classes_performances = df_classes_performances.groupby(['dmal_id_aluno','dmtm_cd_tempo',
    'dmcc_id_componente_curricular']).apply(remove_duplicates)
'''
def check_duplicates (values):
    if values.shape[0] > 1:
        return values['dmal_id_aluno'].iloc[0]
check = df_classes_performances.groupby(['dmal_id_aluno','dmcc_id_componente_curricular','dmtm_cd_tempo'])\
    .apply(check_duplicates)
'''
# algumas notas são nulas quando o aluno reprovou. Como não há uma nota para a reprovação, vou considerar zero
def fill_scores(row):
    value = row['fttu_vl_nota']
    if np.isnan(value) and row['fttu_qt_reprovado'] == 1:
        return 0
    return value
df_classes_performances['fttu_vl_nota'] = df_classes_performances.apply(fill_scores,axis=1)

# montando tabelas

# ESTUDANTE X SEMESTRE
df_students_semesters = df_students.merge(df_students_performances,on=['dmal_id_aluno'],how='inner')
df_students_semesters = df_students_semesters.merge(df_semesters,on=['dmtm_cd_tempo'],how='inner')
key = ['dmal_id_aluno','dmtm_cd_tempo']
df_students_semesters = df_students_semesters.sort_values(by=key)

# transformações e novas variáveis
df_students_semesters['n_semestre_entrada'] = df_students_semesters['dmal_ds_periodo_de_ingresso']\
    .astype(str).apply(lambda x:x[-1]).astype(int)   
df_students_semesters['creditos_adiantados'] = (df_students_semesters['ftda_qt_credito_real'] -\
    df_students_semesters['ftda_nr_media_credito_curso']).clip(0)
df_students_semesters['creditos_atrasados'] =  (df_students_semesters['ftda_nr_media_credito_curso'] -\
    df_students_semesters['ftda_qt_credito_real']).clip(0)
# n_períodos
df_students_semesters['contador'] = 1
df_students_semesters['n_períodos'] = \
    df_students_semesters.groupby('dmal_id_aluno')['contador'].cumsum()
del df_students_semesters['contador']
# n_reprovadas, n_trancadas, n_aprovadas e n_perdidas, n_adiantamentos, n_atrasos
values_to_count = ['fttu_qt_aprovado', 'fttu_qt_reprovado', 'fttu_qt_trancado',
                   'semestres_adiantamento','semestres_atraso',]
df_students_classes = df_students_semesters.merge(df_classes_performances,on=key,how='inner')
df_students_classes = df_students_classes.sort_values(by=key)   

# reduzir dados para que as duas tabelas sejam compatíveis
df_students_semesters = df_students_semesters.merge(df_students_classes[key],on=key).groupby(key).first().reset_index()

'''
qualidade do aluno
média 
0-5 1
5-8 2
8-10 3

'''
def mean_scores(groupby): # desconsiderando notas menores que 5
    return groupby['fttu_vl_nota'].mean()
df_scores = df_students_classes.groupby(key).apply(mean_scores)\
    .rename('média_notas_semestre_anterior').reset_index()
def compute_quality(values):
    range_0_5 = 0
    range_5_8 = 0
    range_8_10 = 0
    for value in values:
        if 0 <= value < 5:
            range_0_5 += 1
        elif (5 <= value < 8):
            range_5_8 += 1
        else:
            range_8_10 += 1
    if range_0_5 >= range_5_8 and range_0_5 >= range_8_10:
        return 0
    if range_5_8 >= range_0_5 and range_5_8 >= range_8_10:
        return 1
    if all(np.isnan(values)):
        return np.nan
    return 2
def compute_quality_semesters(values):
    quality_list = []
    for i in range(1,len(values)+1):
        quality_list.append(compute_quality(values[:i]))
    return quality_list
quality_table = df_scores.groupby(key[0])['média_notas_semestre_anterior'].apply(compute_quality_semesters)
quality_values = []
for quality_row in quality_table:
    for value in quality_row:
        quality_values.append(value)
df_students_semesters['desempenho_aluno'] = quality_values
df_students_semesters['desempenho_aluno_anterior'] = df_students_semesters.groupby(key[0])['desempenho_aluno'].shift(1)

'''
média de notas com cada professor
matriz esparsa de professores
'''
# 
df_student_classes_teachers = \
    df_students_classes.merge(df_teachers,on='dmpr_id_professor',how='inner').sort_values(by=key)   
# quantidade de professores conhecidos pelo nível de dificuldade por semestre
teachers_low_score = [
 'JOSE CARLOS LEITE DOS SANTOS',
 'DANILO DIAS DA SILVA',
 'BRUNO OTAVIO PIEDADE PRADO',
 'ALBERTO COSTA NETO',
 'LEILA MACIEL DE ALMEIDA E SILVA',
 'LEONARDO NOGUEIRA MATOS']
df_student_classes_teachers['é_professor_menor_notas'] =\
    df_student_classes_teachers['dmpr_nm_professor'].isin(teachers_low_score)
df_difficulty = df_student_classes_teachers.groupby(key)['é_professor_menor_notas'].sum()\
    .rename('quantidade_professores_notas_baixas').reset_index()
df_students_semesters = df_students_semesters.merge(df_difficulty,on=key)    
# reduzir o número de professores para os que possuem mais entradas de alunos
teacher_freq = df_student_classes_teachers['dmpr_nm_professor'].value_counts()
#teacher_freq[teacher_freq>150].plot(kind='bar')
frequency = 600
freq_names = teacher_freq[teacher_freq >= frequency].index
#teacher_freq[teacher_freq >= frequency].sort_values().plot(kind='barh',figsize=(5,10))
empty_columns = pd.DataFrame(columns=freq_names)
df_student_classes_teachers = pd.concat([df_student_classes_teachers, empty_columns])
def compute_teacher_columns(row):
    name = row['dmpr_nm_professor']
    if name in freq_names:
        row[name] = row['fttu_vl_nota']
    return row
df_student_classes_teachers = df_student_classes_teachers.apply(compute_teacher_columns,axis=1)
# aplomera resultados de diferentes turmas em um semestre
# computa a media caso haja mais de uma nota
def agg_teacher_values(groupby):
    row = groupby[list(freq_names)].head(1)
    for target in freq_names:
        value_agg = np.NaN # nunca fez
        values = groupby[target].values
        values = values[np.where(~np.isnan(values))]
        if values.shape[0] > 0:
            value_agg = values.mean()
        row[target] = value_agg
        row[target+'_qt'] = values.shape[0]
    return row
df_student_classes_teachers = df_student_classes_teachers.groupby(key).apply(agg_teacher_values).reset_index()
del df_student_classes_teachers['level_2']
# computa a media a cada semestre
def compute_teacher_means(groupby):
    for target in freq_names:
        groupby[target+'_qt'] = groupby[target+'_qt'].cumsum()
        groupby[target] = np.nancumsum(groupby[target]) / groupby[target+'_qt']
        groupby = groupby.drop(target+'_qt',axis=1)
    return groupby
df_student_classes_teachers = df_student_classes_teachers.groupby('dmal_id_aluno').apply(compute_teacher_means)
# propaga valores anteriores
def propagate_values(groupby,base_test):
    for index in np.arange(0,groupby.shape[0]):
        for target in freq_names:
            if index == 0: continue
            if base_test(groupby.iloc[index][target]):
                previous_value = groupby.iloc[index-1][target]
                groupby.loc[groupby.index[index],target] = previous_value
    return groupby
df_student_classes_teachers = df_student_classes_teachers.groupby('dmal_id_aluno').apply(
    lambda x:propagate_values(x,lambda y: np.isnan(y)))
# deslocamento
def shift_values(groupby):
    for target in freq_names:
        groupby[target] = groupby[target].shift(1)
    return groupby
df_student_classes_teachers = df_student_classes_teachers.groupby('dmal_id_aluno').apply(shift_values)
# merge
df_students_semesters = df_students_semesters.merge(df_student_classes_teachers,on=key,how='inner') 
'''
estado de aprovação em disciplina dos semestres anteriores
computa de um semestre, 
    0 nunca fez
    1 0 < nota < 5
    2 5<= nota < 7
    3 7 <= nota <= 10
se nesse semestre não tem a disciplina, colete o valor do semestre anterior
shift os valores para o seguinte
'''
# matriz esparsa de disciplinas
#nome do compomente curricular
df_student_classes_subjects = \
    df_students_classes.merge(df_subjects,on='dmcc_id_componente_curricular',how='inner').sort_values(by=key)   
# quantidade de professores conhecidos pelo nível de dificuldade por semestre
subjects_low_score = ['CIRCUITOS DIGITAIS',
 'RESISTÊNCIA DOS MATERIAIS',
 'PROJETO E ANÁLISE DE ALGORITMOS',
 'FUNDAMENTOS DE MATEMÁTICA',
 'ESTRUTURA DE DADOS I',
 'FÍSICA A',
 'ÁLGEBRA LINEAR I',
 'PROGRAMAÇÃO ORIENTADA A OBJETOS',
 'VETORES E GEOMETRIA ANALÍTICA',
 'CÁLCULO I',
 'CÁLCULO II']
df_student_classes_subjects['é_matéria_menor_notas'] =\
    df_student_classes_subjects['dmcc_nm_componente_curricular'].isin(subjects_low_score)
df_difficulty = df_student_classes_subjects.groupby(key)['é_matéria_menor_notas'].sum()\
    .rename('quantidade_matérias_notas_baixas').reset_index()
df_students_semesters = df_students_semesters.merge(df_difficulty,on=key)  
# reduzir o numero de disciplinas para as que possuem mais entradas de alunos
class_freq = df_student_classes_subjects['dmcc_nm_componente_curricular'].value_counts()
#class_freq[class_freq > 300].plot(kind='bar')
frequency = 600
freq_names = class_freq[class_freq >= frequency].index
#class_freq[class_freq >= frequency].sort_values().plot(kind='barh',figsize=(5,10))
empty_class_columns = pd.DataFrame(columns=freq_names)
df_student_classes_subjects = pd.concat([df_student_classes_subjects, empty_class_columns])
def compute_class_columns(row):
    class_name = row['dmcc_nm_componente_curricular']
    if class_name in freq_names:
        value = 0
        if (row['fttu_qt_reprovado'] == 1) | (row['fttu_qt_trancado']  == 1):
            value = 1
        if row['fttu_qt_aprovado'] == 1 and \
            row['ftda_vl_mgp'] >= 5 and  row['ftda_vl_mgp'] < 7:
            value = 2
        if row['fttu_qt_aprovado'] == 1 and row['ftda_vl_mgp'] >= 7:
            value = 3
        row[class_name] = value
    return row
df_student_classes_subjects = df_student_classes_subjects.apply(compute_class_columns,axis=1)
# todas as classes tem estudantes
#class_columns.isnull().all().any()
# variaveis com nulos
#class_columns.isnull().any()
# preenchendo classes com valor padrão, 0
df_student_classes_subjects = df_student_classes_subjects.fillna(0)
# aplomera resultados de diferentes turmas em um semestre
def agg_class_values(groupby):
    row = groupby[list(freq_names)].head(1)
    for class_target in freq_names:
        value_agg = 0 # se nao se matriculou, default
        values = groupby[class_target].values
        status_values = [3,2,1]
        for possible_status_value in status_values:
            if possible_status_value in values: # se passsou nessa disciplina, marcar passou em todas do semestre
                value_agg = possible_status_value
                break
        row[class_target] = value_agg
    return row
df_student_classes_subjects = df_student_classes_subjects.groupby(key).apply(agg_class_values).reset_index()
'''
#lógica ok
def check_class_columns(groupby):
    row = groupby.head(1)
    del row['dmal_id_aluno']
    for class_target in class_freq_names: # aprovação seguida de reprovação
        found_1 = False    
        for value in groupby[class_target]:
            if value > 1:
                found_1 = True
            if found_1 and value < 2:
                row[class_target] = False
        
        if ((row[class_target] > 1).replace({True:1,False:0}).sum() > 1): # mais de duas aprovações
            row[class_target] =  False
            
        row[class_target] = True
    return row
checagem = class_columns.groupby('dmal_id_aluno').apply(check_class_columns).reset_index()
checagem[list(class_freq_names)].all().all()
'''
del df_student_classes_subjects['level_2']
# propagar valores
df_student_classes_subjects = df_student_classes_subjects.groupby('dmal_id_aluno').apply(
    lambda x:propagate_values(x,lambda y: y==0))
# deslocamento
df_student_classes_subjects = df_student_classes_subjects.groupby('dmal_id_aluno').apply(shift_values)
# preencher os primeiros valores com 0
df_student_classes_subjects = df_student_classes_subjects.fillna(0)
# merge
df_students_semesters = df_students_semesters.merge(df_student_classes_subjects,on=key,how='inner') 
# adiantamentos e atrasos
df_students_classes['semestres_adiantamento'] = df_students_classes['fttu_nr_periodo'] -\
    df_students_classes['n_períodos']    
df_students_classes['semestres_adiantamento'] = \
    df_students_classes['semestres_adiantamento'].clip(0)  # setar valores negativos para zero  
df_students_classes['semestres_atraso'] = df_students_classes['n_períodos'] -\
    df_students_classes['fttu_nr_periodo']
df_students_classes['semestres_atraso'] = \
    df_students_classes['semestres_atraso'].clip(0)  # setar valores negativos para zero         
# contagem e soma cumulativa ao longo dos semestres
df_students_classes['matérias'] = 1
column_count = [  
       'semestres_atraso',
       'semestres_adiantamento',
       'matérias',
       'fttu_qt_reprovado',
       'fttu_qt_aprovado',
       'fttu_qt_trancado',]
counts = df_students_classes.groupby(key)\
    [column_count].sum().reset_index()  # coletando qtdade de disciplinas por semestre
'''
df_students_semesters[df_students_semesters['dmal_id_aluno'] == 7]
counts[counts['dmal_id_aluno'] == 7]
# existem estudantes que, em determinado semestre, 
# não possuem entrada em turma
'''
    
for column in column_count: 
    counts[column+'_semestre'] = counts[column]    
counts['perdidas_semestre'] = counts['fttu_qt_reprovado'] + counts['fttu_qt_trancado']
counts.drop(column_count,axis=1,inplace=True)  # dropando colunas desnecessárias
df_students_semesters = df_students_semesters.merge(counts,on=key,how='inner')  # merge
# total de metricas
column_count = [column+"_semestre" for column in column_count]
column_count += [  
       'creditos_adiantados',
       'creditos_atrasados',  
       'ftda_qt_credito_real',
       'ftda_qt_credito_oficial',
       'perdidas_semestre',]
cum_counts = df_students_semesters.groupby(key[0])[column_count].cumsum()
cum_counts.columns = [name.replace('_semestre','')+'_total' for name in cum_counts.columns]
df_students_semesters = pd.concat([df_students_semesters,cum_counts],axis=1)
# media de metricas
columns_mean = cum_counts.columns
for column in columns_mean:
    df_students_semesters[column.replace("_total","")+'_media'] = df_students_semesters[column] / df_students_semesters['n_períodos']
# métricas do semestre anterior
column_previous = [
 'ftda_vl_mgp','ftda_vl_ir',
 'ftda_qt_credito_real','creditos_adiantados',
 'creditos_atrasados','semestres_atraso_semestre',
 'semestres_adiantamento_semestre','matérias_semestre',
 'fttu_qt_reprovado_semestre','fttu_qt_aprovado_semestre', 'fttu_qt_trancado_semestre',
 'perdidas_semestre',
 'semestres_atraso_media','semestres_adiantamento_media','matérias_media',
 'fttu_qt_reprovado_media', 'fttu_qt_aprovado_media','fttu_qt_trancado_media',
 'ftda_qt_credito_oficial_media', 'perdidas_media'
]
for metric in column_previous:  
    df_students_semesters[metric+'_anterior'] = df_students_semesters.groupby(by=key[0])[metric]\
    .shift(1)    
# idade_aproximada
df_students_semesters['idade_aproximada'] = \
    df_students_semesters['dmtm_nr_ano'] - df_students_semesters['dmal_dt_nascimento']
# curso do aluno
df_students_semesters = df_students_semesters.merge(df_courses,on=['dmcs_id_curso']\
    ,how='inner').drop(['dmcs_id_curso'],axis=1)
'''
abandono de curso
# caso 1, se nao tem entrada por 3 semestres e o total de creditos aprovados é 
menor que o do curso: abandonou
# caso 2, se pegou menos ou igual a 2 disciplinas e nao passou em nenhuma: irregular
'''
df_students_last_semester = df_students_semesters.groupby(['dmal_id_aluno']).last()

def check_dropout(row):   
    last_semester = row['dmtm_cd_tempo'] / 10
    pos = semester_numbers[semester_numbers > last_semester].index[0] - 1
    if (row['dmal_in_concluiu'] == 1 or row['ftda_qt_formado'] == 1):
        return 'formado'
    if (semester_numbers.shape[0] - pos >= 3) and\
        (row['ftda_qt_credito_real_total'] < row['ftda_nr_credito_curso']):
        return "abandonou"
    if (row['matérias_semestre'] <= 2) and (row['fttu_qt_aprovado_semestre'] == 0):
        return "irregular"
    return "regular"
# aplicar na ultima entrada
df_students_last_semester['estado'] = df_students_last_semester.apply(check_dropout,axis=1)
# merge 
df_students_semesters = df_students_last_semester.reset_index()[['dmal_id_aluno','estado']]\
    .merge(df_students_semesters,on=['dmal_id_aluno'],how='inner')
# abandono
df_students_semesters['abandonou'] = df_students_semesters['estado'] == 'abandonou'
df_students_semesters['formado'] = df_students_semesters['estado'] == 'formado'
#df_students_semesters['abandonou'].value_counts()
# equilibrado, porém mais da metade abandonou
#df_students_semesters['abandonou'].value_counts()

'''
df_students_semesters.isnull().any()
# nulos: somente ftda_vl_ir_anterior, n_reprovadas_anterior, ftda_vl_mgp_anterior   
'''
#salvar
df_students_semesters.to_csv(data_path+'tabela_estudante_semestre.csv',sep=";",index=False,encoding="latin-1")


# ESTUDANTE X TURMA
# transformacoes
# dmpr_nm_professor
df_students_classes = df_classes_performances.merge(df_teachers,on=['dmpr_id_professor'],how='inner')
df_students_classes.drop(['dmpr_id_professor'],axis=1,inplace=True)
df_students_classes.rename(columns={
        'fttu_qt_credito_oficial':'crédito_disciplina'},inplace=True)
'''
# curso da disciplina
del df_students_classes['dmcs_id_curso']    
df_students_classes = df_students_classes.merge(
    df_classes[['id_turma','dmcs_id_curso']],on=['id_turma'],how='inner')

df_students_classes = df_students_classes.merge(df_courses,on=['dmcs_id_curso']\
    ,how='inner').drop(['dmcs_id_curso'],axis=1)
df_students_classes.rename(columns={'dmcs_nm_curso':'curso_disciplina'},inplace=True)
'''

# nome da disciplina
df_students_classes = df_students_classes.merge(df_subjects,\
    on=['dmcc_id_componente_curricular'],how='inner').drop(['dmcc_id_componente_curricular'],axis=1)
# variaveis de aluno x semestre
df_students_classes = df_students_classes.merge(df_students_semesters,on=key,how='inner')
'''
df_students_classes.isnull().any().any()
# nulos: somente ftda_vl_ir_anterior, n_reprovadas_anterior, ftda_vl_mgp_anterior   
'''
#salvar
df_students_classes.to_csv(data_path+'tabela_estudante_turma.csv',sep=";",index=False,encoding="latin-1")