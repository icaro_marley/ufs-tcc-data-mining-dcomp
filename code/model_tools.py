# -*- coding: utf-8 -*-
'''
Módulo com funções de apoio aos scripts das etapas do KDD

'''
import numpy as np
import pandas as pd
from sklearn.model_selection import GridSearchCV
import seaborn as sns
import model_tools
from sklearn.model_selection import KFold
from sklearn.pipeline import Pipeline
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)
warnings.simplefilter(action='ignore', category=RuntimeWarning)
from sklearn.preprocessing import Imputer
import matplotlib.pyplot as plt
from xgboost import XGBClassifier
from xgboost import XGBRegressor
from matplotlib.backends.backend_pdf import PdfPages
from sklearn.externals import joblib
from sklearn.base import TransformerMixin, BaseEstimator

results_path = '../results/'
models_path = '../models/'

random_state = 100


def categorize(values,classifications,number):
    previous_value = values[0]
    for index, classification in enumerate(classifications):
        if ((index == 0) and (number <= values[index])) or \
           ((index < len(classifications) - 1) and (previous_value < number <= values[index]))\
           or ((index == len(classifications) - 1) and (number > previous_value)):
            return classification
        previous_value = values[index]
    
# função geral para adaptar um novo dado utilizando o processo e gerar uma previsao
def get_prediction(pre_process, data, model):
    X = pre_process(data)
    return model.predict(X)



'''
Função que treina um modelo para aprendizado supervisionado e gera um relatório
contendo informações sobre o treinamento
Utiliza:
 >Xgboost
 >nested cross-validation (10 fold externo e 5 interno)
 >busca força bruta de hiperparâmetros na validação interna
'''

class ImputerDataframe(TransformerMixin, BaseEstimator):
    def __init__(self,imputer,columns):
        self.imputer = imputer
        self.columns = columns
    
    def fit(self,X, *args):
        self.imputer.fit(X)
        self.mask = ~ np.isnan(self.imputer.statistics_)
        return self
        
    def transform(self,X):
        X = self.imputer.transform(X)
        return pd.DataFrame(X,columns=self.columns[self.mask])



def train_summarize(data, X_columns, target, task, score_func, summary_name):
    if task == "classification":
        model = XGBClassifier(random_state=model_tools.random_state,n_jobs=-1)
    else:
        model = XGBRegressor(random_state=model_tools.random_state,n_jobs=-1)
        
    # dummies
    X_dummies = pd.get_dummies(data[X_columns])
    X_columns_dummies = X_dummies.columns
    X_train = X_dummies.values
    y_train = data[target].values
    
    model = Pipeline([
                ('imputer',ImputerDataframe(
                    imputer=Imputer(strategy='most_frequent'),
                    columns=X_columns_dummies)),
                ('model', model),
    ])
    
    params = {  
            'model__learning_rate':[0.1,0.2,0.3],
            'model__max_depth':[3,7,11],
            'model__n_estimators':[100,150,200],
            'model__reg_alpha':[0,10,20,100],
            'model__gamma':[0,5,15],
    }
    
    k_fold = KFold(5,random_state=model_tools.random_state)
    inner_grid = GridSearchCV(model,cv=k_fold, 
                param_grid=params,scoring=score_func,
                verbose=1,n_jobs=1)
    
    k_fold = KFold(10,random_state=model_tools.random_state) 
    outer_grid = GridSearchCV(inner_grid,cv=k_fold, 
                param_grid={},scoring=score_func,
                verbose=0,n_jobs=1)
    
    grid = outer_grid
    
    # treinamento
    grid.fit(X_train,y_train)
    
    # resultados: pontuações e importâncias
    importances = grid.best_estimator_.best_estimator_.named_steps['model']\
        .get_booster().get_score(importance_type='gain')
    df_importances = pd.DataFrame(list(importances.values()),index=list(importances.keys()),columns=['gain'])
    df_importances = df_importances[df_importances['gain']>0]
    df_importances.sort_values(by='gain',inplace=True,ascending=False)
    df_importances = df_importances.head(5)

    # Relatório em pdf
    pp = PdfPages(results_path + summary_name +'_summary.pdf')

    # Modelo e hiperparâmetros
    param_list = str(grid.best_estimator_.best_estimator_.named_steps['model'].get_params()).\
        replace("'","").replace(" ","").replace("{","").replace("}","").\
            split(',')
    fig, ax = plt.subplots(1)
    fig.text(0,0.9,'Model: ' + 'Xgboost')
    fig.text(0,0.8,'Parameters: ' + str(param_list[:3]).replace('[','').replace(']',''))
    i = 3
    for y_pos in np.arange(0.7,0.0,-0.1):
        fig.text(0,y_pos, str(param_list[i:i+4]).replace('[','').replace(']',''))
        i+= 4
    plt.axis('off')
    pp.savefig(fig)
    plt.show()
    
    # Pontuações
    fig, ax = plt.subplots(1)
    fig.text(0,0.9,'Pontuações (' + score_func.__name__ + '):')
    fig.text(0,0.8,'Train Score: {:.2f} +- {:.2f}'.\
        format(grid.cv_results_['mean_train_score'][0],
        grid.cv_results_['std_train_score'][0]))
    fig.text(0,0.7,'Test Score: {:.2f} +- {:.2f}'.\
        format(grid.cv_results_['mean_test_score'][0],
        grid.cv_results_['std_test_score'][0]))
    plt.axis('off')
    pp.savefig(fig)
    plt.show()
    
    # Importâncias
    fig, ax = plt.subplots(1)
    sns.barplot(df_importances['gain'],df_importances.index,color='steelblue',ax=ax)
    plt.xlabel('Importância')
    plt.ylabel('Variável')
    plt.title("Importância das variáveis")
    plt.tight_layout()
    pp.savefig(fig)
    plt.show()

    # PDP com plot dos dados
    predict_func = grid.predict
    if task == 'classification':
        predict_func = grid.predict_proba
    data_ = pd.get_dummies(data[X_columns]).copy()
    for important_feature in df_importances.index:
        print(important_feature)
        fig, ax = plt.subplots(1)
        # pdp
        x1 = []
        y1 = []
        min_value = np.nanmin(data_[important_feature])
        max_value = np.nanmax(data_[important_feature])
        step = (max_value - min_value)/ 100
        for index,i in enumerate(np.arange(min_value, max_value, step)):
            data_modified = data_.copy()
            data_modified[important_feature] = i
            new_y = predict_func(data_modified)
            if task == 'classification':
                new_y = new_y[:,1]
            x1 += [i]
            y1 += [np.mean(new_y)]
        plt.plot(x1,y1,'r-',c='steelblue')
        # dados
        jitter_x = [0] * len(y_train)
        jitter_y = jitter_x
        if task == 'classification':
            jitter_x = np.random.randn(len(y_train)) / 100
            jitter_y = np.random.randn(len(y_train)) / 100
        imputed_X_train = grid.best_estimator_.best_estimator_.named_steps['imputer'].transform(X_train)
        plt.scatter(imputed_X_train[important_feature]+jitter_x,y_train+jitter_y,alpha=0.05,s=6,c='darkred')
        plt.gca().legend(('Média das previsões','Dados'))
        plt.ylabel(target)
        plt.title("PDP e dados: " + important_feature)
        pp.savefig(fig)
        
        plt.show()
        
    pp.close()    
   
    # Salva modelo
    joblib.dump(grid.best_estimator_, models_path + summary_name + '_model.pkl')
    
    return grid