'''
4- Quais săo os padrőes nos alunos que reprovam mais de 1 vez na disciplina que possuui maior taxa de reprovaçăo?


arm em cima dos últimos dados do aluno x turma

restringindo o cálculo taxa de reprovação para apenas da primeira tentativa
'''

# imports
import pandas as pd
import fim
import numpy as np
from model_tools import categorize

data_path = '../data/modified data/DadosBancoCSV/'
results_path = '../results/'


df_students_classes = pd.read_csv(data_path+'tabela_estudante_turma.csv',encoding="latin-1",sep=';',low_memory=False)
    
df_students_classes = df_students_classes\
    .sort_values(['dmal_id_aluno','n_períodos'],ascending=True)

# soma da primeira reprovacao de cada aluno
def get_first_failure(values):
    if values[values==1].shape[0] >= 1:
        return 1
    return 0
failure_counts = df_students_classes.groupby(['dmal_id_aluno','dmcc_nm_componente_curricular'])['fttu_qt_reprovado'].apply(get_first_failure).reset_index()
failure_counts = failure_counts.groupby('dmcc_nm_componente_curricular')['fttu_qt_reprovado'].sum()
subject_failure = failure_counts.sort_values(ascending=False).index[0]
'''
failure_counts = failure_counts.sort_values(ascending=False)[:20]
import seaborn as sns
sns.barplot(failure_counts,failure_counts.index,color='lightskyblue')
'''

# transformacoes 
groubed_by_student = df_students_classes\
    .groupby(['dmal_id_aluno'])
df_students_high_failure = groubed_by_student.last() # coletar os ultimos dados dos estudantes

# marcar se o aluno reprovou mais de uma vez
def check_has_second_try(values):
    values = values[values['dmcc_nm_componente_curricular'] == subject_failure]
    if values['fttu_qt_reprovado'].sum() > 1:
        return True
    return False
df_students_high_failure['reprovou_mais_de_1'+subject_failure] = df_students_classes.groupby(
    'dmal_id_aluno').apply(check_has_second_try)
df_students_high_failure['reprovou_mais_de_1'+subject_failure] =\
 df_students_high_failure['reprovou_mais_de_1'+subject_failure].replace({True:"Reprovações > 1",False:"Reprovações <= 1"})

# tentativas
def check_n_tries(values):
    values = values[values['dmcc_nm_componente_curricular'] == subject_failure]
    return values['fttu_qt_reprovado'].sum() + values['fttu_qt_trancado'].sum() + values['fttu_qt_aprovado'].sum()
df_students_high_failure['n_tentativas_'+subject_failure] = df_students_classes.groupby(
    'dmal_id_aluno').apply(check_n_tries)
categories = lambda x:categorize(
    [1, 2],
    ["tentativas <= 1", "1 < tentativas <= 2",
     "tentativas > 2"],x)
df_students_high_failure['n_tentativas_'+subject_failure] = df_students_high_failure['n_tentativas_'+subject_failure]\
                    .apply(categories)
# aprovado
def check_success(values):
    values = values[values['dmcc_nm_componente_curricular'] == subject_failure]
    if any(values['fttu_qt_aprovado'] == 1):
        return "foi aprovado"
    return "não foi aprovado"
df_students_high_failure['aprovado_disciplina_'+subject_failure] = df_students_classes.groupby(
    'dmal_id_aluno').apply(check_success)

# professor que mais deu a disciplina pro aluno
def check_frequent_teacher(values):
    values = values[values['dmcc_nm_componente_curricular'] == subject_failure]
    teachers = values['dmpr_nm_professor'].value_counts(ascending=False)
    if teachers.shape[0]==0:
        return '' 
    if teachers[0] == 1:
        return ''
    return teachers.index[0]
df_students_high_failure['professor_moda_'+subject_failure] = df_students_classes.groupby(
    'dmal_id_aluno').apply(check_frequent_teacher)

# tentativas e sucesso
def check_success_tries(row):
    classification2 = row['n_tentativas_'+subject_failure]
    classification1 = row['aprovado_disciplina_'+subject_failure] 
    return classification1 + " e " + classification2 
df_students_high_failure['tentativas_e_sucesso_'+subject_failure] = \
    df_students_high_failure.apply(check_success_tries,axis=1)

# idade
categories = lambda x: \
        categorize([1988,1992], [
        "nascimento <= 1988 ",
        "1988 < nascimento <= 1992",    
        "nascimento > 1992",
        ], x)
df_students_high_failure['dmal_dt_nascimento'] = \
    df_students_high_failure['dmal_dt_nascimento'].apply(categories)

categories = lambda x: \
        categorize([0.15,0.5], [
        "regularidade <= 0.15",
        "0.15 < regularidade <= 0.5", 
        "regularidade > 0.5",
        ], x)
df_students_high_failure['ftda_vl_ir'] =\
    df_students_high_failure['ftda_vl_ir'].apply(categories)
        
categories = lambda x: \
        categorize([4.9,8], [
        "média <= 5",
        "5 < média <= 8", 
        "média > 8",       
        ], x)
df_students_high_failure['ftda_vl_mgp'] = \
    df_students_high_failure['ftda_vl_mgp'].apply(categories)

categories = lambda x: \
        categorize([2010.1,2013.1], [
        "entrada <= 2010.1",
        "2010.1 < entrada <= 2013.1", 
        "entrada > 2013.1",       
        ], x)
df_students_high_failure['dmal_ds_periodo_de_ingresso'] = \
    df_students_high_failure['dmal_ds_periodo_de_ingresso'].apply(categories)

# estadia do aluno
categories = lambda x:categorize(
    [4, 8],
    ["estadia <= 4", "4 < estadia <= 8", 
     "estadia > 8"],x)
df_students_high_failure['estadia_aluno'] = df_students_high_failure['n_períodos']\
                    .apply(categories)

df_students_high_failure['entrada_estadia'] = \
    df_students_high_failure['estadia_aluno'] + ' e ' +\
        df_students_high_failure['dmal_ds_periodo_de_ingresso'] 

df_students_high_failure['dmcs_nm_curso'] = df_students_high_failure['dmcs_nm_curso']\
    .replace({"Ciência Da Computação ":"Ciência da Computação",
    "Engenharia de Computação ":"Engenharia da Computaçao"})

#
to_replace = {
1:'escola pública',
2:'escola particular ou pública',
3:'superior incompleto',
}
df_students_high_failure['n_escolaridade'] = df_students_high_failure['n_escolaridade'].replace(to_replace)

'''
df_students_high_failure['reprovou_mais_de_1'+subject_failure].value_counts()/df_students_high_failure.shape[0]

5 <= média <= 8    0.721328 0.72%

'professor_moda_'+subject_failure possui poucos valores, desconsiderar.

reprovações
Reprovações <= 1    0.804829
Reprovações > 1     0.195171

sexo
Masculino    0.872736
Feminino     0.127264

escolaridade
escola particular ou pública 53.5%
'''
    
# seleção de variaveis
columns = [
       'dmcs_nm_curso',
       'ftda_vl_mgp',
       'dmal_dt_nascimento',
       'dmal_ds_cota',
       'dmal_ds_sexo',
       'estado',
       'n_escolaridade',
       'reprovou_mais_de_1'+subject_failure,
]

df_students_high_failure = df_students_high_failure[columns]

'''
Suporte baixo para permitir encontrar regras que possuam alta confiança 
Confiança baixa para conseguir pegar as regras que possuem a variável alvo (Tentativas > 1)
'''
# Fp growth
values = df_students_high_failure.astype(str).values
rules = fim.fpgrowth(values,target='r',supp=3, conf=40, report='(SXYCl')
df_results = pd.DataFrame(rules,columns=['consequent','antecedent','metrics'])
df_results = df_results[['antecedent','consequent','metrics']]
df_results[['item set support','body set support','head support','confidence','lift']] = pd.DataFrame(np.array(list(df_results['metrics'].values)))
del df_results['metrics']
df_results['rule'] = df_results['antecedent'].astype(str).str.replace("'","") + " => " + df_results['consequent'].astype(str).str.replace("'","")

# prunning
df_rules = df_results[df_results['consequent'].isin(df_students_high_failure['reprovou_mais_de_1'+subject_failure].unique())].copy()
df_rules['antecedent'] = df_rules['antecedent'].apply(list)
'''
remover regras com 'Masculino' no antecedent
remover regras com 'Ciência da Computação' no antecedent
remover regras com 'sem sistema de cotas' no antecedent, pois esses alunos são de um sistema antigo 
e a tendência é esse atributo sumir do 
remover regras 'escola particular ou pública' por causa da grande quantidade de registros que a possui
'''
df_rules = df_rules[~df_rules['antecedent'].apply(lambda x: 'Masculino' in x)]
df_rules = df_rules[~df_rules['antecedent'].apply(lambda x: '5 < média <= 8' in x)]
df_rules = df_rules[~df_rules['antecedent'].apply(lambda x: 'sem sistema de cotas' in x)]
df_rules = df_rules[~df_rules['antecedent'].apply(lambda x: 'escola particular ou pública' in x)]

# lift
df_rules = df_rules[df_rules['lift'] > 1]

'''
Não foram encontradas regras para os alunos que reprovaram mais de uma vez que possuíssem valores significativos para confiança e suporte
(confinaça abaixo de 40%)
'''

# prints
print("Número de regras encontradas:",len(df_rules))
df_rules.to_csv(results_path+'q4-fp_growth-results.csv',sep=";",index=False,encoding="latin-1")
