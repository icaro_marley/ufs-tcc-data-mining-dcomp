# -*- coding: utf-8 -*-
"""
5-Quais estudantes estão em risco de largar os cursos?
"""

import pandas as pd
from model_tools import train_summarize
from sklearn.metrics import matthews_corrcoef

'''
df_students_last_semester['dmcs_nm_curso'].value_counts()
Ciência Da Computação        1066
Sistemas de Informação        492
Engenharia de Computação      430
como existem poucos alunos nos outros dois cursos, vou considerar o grupo inteiro de dados 
e treinar um modelo só
'''

data_path = '../data/modified data/DadosBancoCSV/'
df_students_semesters = pd.read_csv(data_path+'tabela_estudante_semestre.csv',encoding="latin-1",sep=';',low_memory=False)

# ultimos dados dos estudantes
df_students_last_semester = df_students_semesters.groupby(['dmal_id_aluno']).last()

def matthews_correlation_coef(model, X_pred, y_true,):
    return matthews_corrcoef(y_true,model.predict(X_pred))

target = 'abandonou'

'''
seleção de variaveis
Considerando que o aluno pode abandonar durante o semestre,
 é interessante identificar se ele vai abandonar 
 durante o novo semestre.

'''

# codificando target
df_students_last_semester[target] = df_students_last_semester[target].replace({True:1,False:0})

list(df_students_last_semester.columns)

X_columns = [
 # informações do aluno
 'dmcs_nm_curso',
 'n_escolaridade',
 'dmal_dt_nascimento',
 'dmal_ds_sexo',
 'dmal_ds_forma_de_ingresso',
 'dmal_ds_tipo_necessidade_especial',
 'n_semestre_entrada',

 # informações do semestre
 'quantidade_professores_notas_baixas', 
 'quantidade_matérias_notas_baixas',
 'semestres_atraso_semestre',
 'ftda_qt_credito_oficial',
 'creditos_atrasados',
 'n_períodos',

 # métricas do aluno até o semestre
 'semestres_atraso_media',
 'ftda_qt_credito_oficial_media',
 'HENRIQUE NOU SCHNEIDER',
 'ANTONIO MONTEIRO FREIRE',
 'LUIZ BRUNELLI',
 'MARCO TULIO CHELLA',
 'EDILAYNE MENESES SALGUEIRO',
 'TARCISIO DA ROCHA',
 'LEONARDO NOGUEIRA MATOS',
 'MARIA AUGUSTA SILVEIRA NETTO NUNES',
 'ADMILSON DE RIBAMAR LIMA RIBEIRO',
 'LEILA MACIEL DE ALMEIDA E SILVA',
 'HENDRIK TEIXEIRA MACEDO',
 'KENIA KODEL COX',
 'RICARDO JOSE PAIVA DE BRITTO SALGUEIRO',
 'ALBERTO COSTA NETO',
 'CARLOS ALBERTO ESTOMBELO MONTESCO',
 'BRUNO OTAVIO PIEDADE PRADO',
 'ANDRE BRITTO DE CARVALHO',
 'DANIEL OLIVEIRA DANTAS',
 'CÁLCULO I',
 'VETORES E GEOMETRIA ANALÍTICA',
 'PROGRAMAÇÃO IMPERATIVA',
 'PROGRAMAÇÃO ORIENTADA A OBJETOS',
 'FUNDAMENTOS DE MATEMATICA PARA COMPUTACAO',
 'ÁLGEBRA LINEAR I',
 'FÍSICA A',
 'ESTATISTICA APLICADA',
 'CÁLCULO II',
 'REDES DE COMPUTADORES I',
 'INFORMÁTICA ÉTICA E SOCIEDADE',
 'INTERFACE HUMANO - COMPUTADOR',
 'ESTRUTURA DE DADOS I',
 'FUNDAMENTOS DA COMPUTAÇÃO',
 'MÉTODOS E TÉCNICAS DE PESQUISA',
 'CIRCUITOS DIGITAIS I',
 'FÍSICA B',
 'INTRODUCAO A ADMINISTRACAO',
 'PROGRAMAÇÃO PARA WEB',
 'INGLÊS INSTRUMENTAL',
 'CÁLCULO NUMÉRICO I',
 'INFORMÁTICA EDUCATIVA',
 'LABORATÓRIO DE CIRCUITOS DIGITAIS I',
 'BANCO DE DADOS', 
 'ftda_vl_mgp_anterior',
 'ftda_vl_ir_anterior',
 'ftda_qt_credito_real_anterior',
 'desempenho_aluno_anterior',
]

len(X_columns)

train_summarize(df_students_last_semester, X_columns, target, 'classification', matthews_correlation_coef, 'q5')

print('Fim do treinamento. Checar arquivos gerados')


'''
As avaliações são feitas em torno das previsões do modelo considerando os valores (apenas) da variável em questão.

overfitting: mais de quase .3 de diferença
desvio padrão alto 0.07 no teste
pontuação de 0.66, boa

Nota na disciplina Interface Humano - Computador, créditos atrasados, nota nas disciplians Banco de Dados, Redes de Computadores 1 e nota média com o professor Ricardo Jose Paiva de Britto Salgueiro contribuem bastante para o modelo
uma maior nota em Interface Humano - Computador diminui a chance de abandono
uma maior quantidade de créditos atrasados aumenta a chance de abandono
estado na disciplina Banco de Dados influencia a previsão, porém pouco
um melhor estado na disciplina Redes de Computadores 1 esta associado com uma menor chance de abandono
maiores notas com o professor Ricardo Jose Paiva Britto Salgueiro geram uma menor chance de abandono



data de nascimento quando mais jovem menos chance
numero de periodos quanto maior menos chance
credito oficial media quanto maior menos chance e poucos valores para acima de 80
ir anterior pouca influência
creditos atrasados quanto maior mais chance
'''