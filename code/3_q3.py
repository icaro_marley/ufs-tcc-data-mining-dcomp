# -*- coding: utf-8 -*-

'''
3- "Qual é a chance de um aluno reprovar em sua primeira tentativa em uma
 das matérias com maior taxa de reprovação?"

3.1- Qual é a chance de um aluno reprovar em qualquer uma das disciplinas 'ESTRUTURA DE DADOS I' e 'PROJETO E ANÁLISE DE ALGORITMOS' ?

'''

import pandas as pd
from sklearn.metrics import matthews_corrcoef
from model_tools import train_summarize

data_path = '../data/modified data/DadosBancoCSV/'

'''
encontrar materias que mais reprovam
realizar tratamentos
montar um dataset especifico para elas
'''

df_students_classes = pd.read_csv(data_path+'tabela_estudante_turma.csv',encoding="latin-1",sep=';',low_memory=False)
    
'''
considerar apenas entradas de 1 tentativa no cálculo da disciplina que mais reprova
'''
grouped_by_classes = df_students_classes.groupby(['dmcc_nm_componente_curricular',
    'dmal_id_aluno']).first().reset_index().\
    groupby('dmcc_nm_componente_curricular')['fttu_qt_reprovado'].sum().reset_index()
# as 3 primeiras disciplinas
df_classes = grouped_by_classes.sort_values(by=['fttu_qt_reprovado'], ascending=False)[:3]

'''
selecionar disciplinas extras pedidas 

25                                   CÁLCULO I              747.0
193              VETORES E GEOMETRIA ANALÍTICA              632.0
61   FUNDAMENTOS DE MATEMATICA PARA COMPUTACAO              532.0

'''

extra = ['ESTRUTURA DE DADOS I','PROJETO E ANÁLISE DE ALGORITMOS']
extra_mask = grouped_by_classes['dmcc_nm_componente_curricular'].isin(extra)
extra_classes = grouped_by_classes[extra_mask]


df_classes = df_classes.append(extra_classes)

# selecionando essas disciplinas no arquivo ALUNOS X TURMA
student_failure_classes_mask = df_students_classes['dmcc_nm_componente_curricular']\
    .isin(df_classes['dmcc_nm_componente_curricular'])
df_student_failure_classes = df_students_classes[student_failure_classes_mask]
del student_failure_classes_mask, grouped_by_classes,\
    df_students_classes, df_classes, extra_classes, extra_mask, extra

'''
tratamentos e transformacoes
'''
#considerar apenas entradas de 1 tentativa, 
df_student_failure_classes = df_student_failure_classes\
    .sort_values(['dmal_id_aluno','n_períodos'],ascending=True)\
    .groupby(['dmal_id_aluno','dmcc_nm_componente_curricular']).first().reset_index()

'''
# quantidade de alunos em cada turma
df_student_failure_classes.groupby('dmcc_nm_componente_curricular')['dmal_id_aluno'].count()
'''

target = 'fttu_qt_reprovado'

def matthews_correlation_coef(model, X_pred, y_true,):
    return matthews_corrcoef(y_true,model.predict(X_pred))

# seleção
'''
para disciplinas de primeiro semestre

'dmcs_nm_curso',
 'dmal_dt_nascimento',
 'dmal_ds_sexo',
 'dmal_ds_tipo_necessidade_especial',
 'dmpr_nm_professor',
 'dmpr_tp_professor',
 'n_semestre',
 'n_escolaridade',

para as outras: + metricas anteriores
professor
disciplinas
metricas gerais
'''

first_semester_columns = [ 
 # informações básicas do aluno
 'dmcs_nm_curso',
 'n_escolaridade',
 'dmal_dt_nascimento',
 'dmal_ds_sexo',
 'dmal_ds_forma_de_ingresso',
 'dmal_ds_tipo_necessidade_especial',
 'n_semestre_entrada',
 
 # informações do semestre
 'quantidade_professores_notas_baixas',
 'quantidade_matérias_notas_baixas',
 
 # informações da turma  
 'dmpr_nm_professor',
 'dmpr_tp_professor',
 ]

non_first_semester_columns = first_semester_columns + [
 # informações do semestre        
 'creditos_atrasados',
 'n_períodos',
 'semestres_atraso_semestre',
 'ftda_qt_credito_oficial',

 # métricas do aluno até o semestre
 'ftda_qt_credito_oficial_media',
 'semestres_atraso_media',
 'HENRIQUE NOU SCHNEIDER',
 'ANTONIO MONTEIRO FREIRE',
 'LUIZ BRUNELLI',
 'MARCO TULIO CHELLA',
 'EDILAYNE MENESES SALGUEIRO',
 'TARCISIO DA ROCHA',
 'LEONARDO NOGUEIRA MATOS',
 'MARIA AUGUSTA SILVEIRA NETTO NUNES',
 'ADMILSON DE RIBAMAR LIMA RIBEIRO',
 'LEILA MACIEL DE ALMEIDA E SILVA',
 'HENDRIK TEIXEIRA MACEDO',
 'KENIA KODEL COX',
 'RICARDO JOSE PAIVA DE BRITTO SALGUEIRO',
 'ALBERTO COSTA NETO',
 'CARLOS ALBERTO ESTOMBELO MONTESCO',
 'BRUNO OTAVIO PIEDADE PRADO',
 'ANDRE BRITTO DE CARVALHO',
 'DANIEL OLIVEIRA DANTAS',
 'CÁLCULO I',
 'VETORES E GEOMETRIA ANALÍTICA',
 'PROGRAMAÇÃO IMPERATIVA',
 'PROGRAMAÇÃO ORIENTADA A OBJETOS',
 'FUNDAMENTOS DE MATEMATICA PARA COMPUTACAO',
 'ÁLGEBRA LINEAR I',
 'FÍSICA A',
 'ESTATISTICA APLICADA',
 'CÁLCULO II',
 'REDES DE COMPUTADORES I',
 'INFORMÁTICA ÉTICA E SOCIEDADE',
 'INTERFACE HUMANO - COMPUTADOR',
 'ESTRUTURA DE DADOS I',
 'FUNDAMENTOS DA COMPUTAÇÃO',
 'MÉTODOS E TÉCNICAS DE PESQUISA',
 'CIRCUITOS DIGITAIS I',
 'FÍSICA B',
 'INTRODUCAO A ADMINISTRACAO',
 'PROGRAMAÇÃO PARA WEB',
 'INGLÊS INSTRUMENTAL',
 'CÁLCULO NUMÉRICO I',
 'INFORMÁTICA EDUCATIVA',
 'LABORATÓRIO DE CIRCUITOS DIGITAIS I',
 'BANCO DE DADOS', 
 'ftda_vl_mgp_anterior',
 'ftda_vl_ir_anterior',
 'ftda_qt_credito_real_anterior',
 'desempenho_aluno_anterior',
]

class_names = [
    'CÁLCULO I',
    'VETORES E GEOMETRIA ANALÍTICA',
    'FUNDAMENTOS DE MATEMATICA PARA COMPUTACAO',
    'PROJETO E ANÁLISE DE ALGORITMOS',
    'ESTRUTURA DE DADOS I'           
]

for class_name in class_names:
    print(class_name)
    df_class = df_student_failure_classes[df_student_failure_classes[
    'dmcc_nm_componente_curricular'] == class_name]
    X_columns = first_semester_columns
    if class_name in ['PROJETO E ANÁLISE DE ALGORITMOS', 'ESTRUTURA DE DADOS I']:
        X_columns = non_first_semester_columns
    train_summarize(df_class, X_columns, target, 'classification', matthews_correlation_coef, 'q3_' + class_name)

print('Fim do treinamento. Checar arquivos gerados')

'''
As avaliações são feitas em torno das previsões do modelo considerando os valores (apenas) da variável em questão.

C1: 
.28, alto desvio padrão
nível de escolaridade e nome do professor exercem mais influencia sobre o modelo
um maior nível de escolaridade parece ter associação com uma menor chance de reprovação, porém não existem muitos dados de níveis altos que fortaleçam essa hipótese e por isso, forma-se um padrão não linear nas previsões do modelo
alunos da professora Joselia Santos Moura possuem uma menor chance de reprovação
alunos da professores Douglas Ferreira de Albuquerque e Luiz Carlos Dantas Santos não possuem grandes efeitos nas decisões do modelo
alunos da professora Danielle de Carvalho Soares possuem uma menor chance de reprovação

VGA:
.32, melhor que C1. desvio padrão alto, .07
nome do professor e semestre de entrada do aluno possuem maior influência sobre as decisões do modelo
alunos do professores Denisson de Oliveira Libório, Marcos Hernani Silva Santos e Alexandro da Silva Neo possuem menor chance de reprovação
alunos do segundo semestre possuem maior chance de reprovação, embora a diferença não seja tão grande
alunos do professor Paulo de Souza Rabelo possuem uma maior chance de reprovação

FMA:
modelo mais simples
.15 de pontuação, péssimo. std alta, quase .15 de diferença entre o treinamento e teste
nome do professor e data de nascimento infulenciam mais as previsões do modelo
aluno do professor Danilo Dias da Silva possue uma maior chance de reprovar
alunos com nascimento acima dos anos 90 possuem uma menor chance de reprovar
as outras 3 variáveis não influenciam muito as decisões do modelo

PAA:
.66, std alta: .12
quase .4 de diferença entre teinamento e teste, porém a pontuação de teste ainda é alta e torna o modelo aceitável
créditos atrasados, nome do professor, notas com os professores Daniel Oliveira Dantas e Leila Maciel de Almeida e Silva e ir anterior influenciam as previsões
alunos com mais creditos atrasados no semestre possuem mais chance de reprovar
o fato da disciplina ser ministrada pelo professor Bruno Otavio não influencia as previsões do modelo
o mesmo ocorre com notas com Daniel Oliveira e ir anterior
alunos que possuem nota média alta com a professora Leila Maciel de Almeida e Silva, possuem maiores chances de reprovar, porém faltam dados para explicar o fenomeno com segurança.

ED1:
.64, std alta .15, quase .4 de diferença entre teste e treinamento. Similarmente ao modelo de PAA, o modelo é aceitável
créditos atrasados, nome do professor, notas com os professores Alberto Costa Neto e Antonio Monteiro Freire influenciam bastante as decisões do modelo
maior quantidade de créditos atrasados do semestre está associada com maior chance de reprovar
alunos do professor Eugenio Rubens Cardoso Braz possuem uma menor chance de reprovar
alunos do professor Alberto Costa Neto possuem uma maior chance de reprovar, porém a diferença não é grande
nota média alta em alunos do professor Alberto Costa Neto está associada com uma maior chance de repovar, porém faltam dados para entender se o padrão realment existe
média de notas com o professor Antonio MOnteiro Freire não influenciam muito as decisões do modelo


existe uma diferença entre as pontuações dos modelos das disciplinas de primeiro e dos outros semestres. A diferença se dá por conta das variáveis que podem ser utuilizadas a partir do segundo semestre, além de que o desempenho do aluno se estabiliza ao longo dos semestres, através das medidas de média de desempenho

é importante notar a influência da variável nome do professor nas primeiras disciplinas
Embora quase não existam caraterísticas sobre o desempenho do aluno no primeiro semestre, é possível conseguir um nível aceitavel de previsão usando essas variáveis.

'''