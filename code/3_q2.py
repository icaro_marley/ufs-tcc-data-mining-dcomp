# -*- coding: utf-8 -*-
'''
pergunta: quais sao os padroes nos estudantes que não frequentam os cursos?

http://www.borgelt.net/pyfim.html

redudantes 
http://mhahsler.github.io/arules/reference/is.redundant.html 
http://www.ijcse.com/docs/INDJCSE12-03-06-073.pdf
Overview of Non-redundant Association Rule Mining
'''

# imports
import pandas as pd
import numpy as np
import fim
from model_tools import categorize

data_path = '../data/modified data/DadosBancoCSV/'
results_path = '../results/'

# categoriza um numero de acoro com um range de valores e classificacoes     
# (a,b) fechado em a, aberto em b


df_students_semesters = pd.read_csv(data_path+'tabela_estudante_semestre.csv',encoding="latin-1",sep=';',low_memory=False)
    
# ultimos dados dos estudantes
df_students_last_semester = df_students_semesters.groupby(['dmal_id_aluno']).last()
del df_students_semesters
'''
dmal_ds_tipo_necessidade_especial nenhum possui necessidade especial, ignorando variável
n_periodos qt reprovado qttrancado n faz sentido pois 
'''

# tratamento de tipos
# curso do aluno
df_students_last_semester['dmcs_nm_curso'] = df_students_last_semester['dmcs_nm_curso']\
    .replace({"Ciência Da Computação ":"Ciência da Computação",
    "Engenharia de Computação ":"Engenharia da Computaçao"})
'''
dividindo em quantidades similares representadas por 3 valores
'''
# estadia do aluno
categories = lambda x:categorize(
    [4, 8],
    ["estadia <= 4", "4 < estadia <= 8", 
     "estadia > 8"],x)
df_students_last_semester['estadia_aluno'] = df_students_last_semester['n_períodos']\
                    .apply(categories)

# idade
categories = lambda x: \
        categorize([1988,1992], [
        "nascimento <= 1988 ",
        "1988 < nascimento <= 1992",    
        "nascimento > 1992",
        ], x)
df_students_last_semester['dmal_dt_nascimento'] = df_students_last_semester['dmal_dt_nascimento'].apply(categories)

'''
desconsiderando (alto desequilíbrio)
dmal_ds_forma_de_ingresso
'''
categories = lambda x: \
        categorize([2010.1,2013.1], [
        "entrada <= 2010.1",
        "2010.1 < entrada <= 2013.1", 
        "entrada > 2013.1",       
        ], x)
df_students_last_semester['dmal_ds_periodo_de_ingresso'] = df_students_last_semester['dmal_ds_periodo_de_ingresso'].apply(categories)

categories = lambda x: \
        categorize([4.9,8], [
        "média <= 5",
        "5 < média <= 8", 
        "média > 8",       
        ], x)
df_students_last_semester['ftda_vl_mgp'] = df_students_last_semester['ftda_vl_mgp'].apply(categories)

del categories

to_replace = {
1:'período 1',
2:'período 2',
}
df_students_last_semester['n_semestre_entrada'] = df_students_last_semester['n_semestre_entrada'].replace(to_replace)

to_replace = {
1:'escola pública',
2:'escola particular ou pública',
3:'superior incompleto',
}
df_students_last_semester['n_semestre_entrada'] = df_students_last_semester['n_semestre_entrada'].replace(to_replace)

df_students_last_semester['entrada_estadia'] = df_students_last_semester['estadia_aluno'] + ' e ' + df_students_last_semester['dmal_ds_periodo_de_ingresso'] 

# selecionando colunas
columns = [
 'estado',
 'entrada_estadia',
 'dmal_ds_sexo',
 'dmal_ds_cota',
 'ftda_vl_mgp',
 'dmal_dt_nascimento',
 'dmcs_nm_curso',
 'n_semestre_entrada',
 'n_escolaridade',
]


df_students_selected = df_students_last_semester[columns].copy()
del columns, df_students_last_semester
#df_students_selected.isnull().any().any()
#  0 nulos

'''
considerando frequencia como regularidade dos que não se formaram,
tratar alunos que estão irregulares ou abandonaram os cursos como um só

'''
df_students_selected['estado'] = df_students_selected['estado'].replace({'abandonou':'irregular'})


'''
(df_students_selected['estado'].value_counts() / df_students_selected.shape[0])[0]
irregular é .46% dos dados

87.2% dos dados são de alunos do sexo masculino

df_students_selected['dmal_ds_sexo'].value_counts() / df_students_selected.shape[0]


df_students_selected['ftda_vl_mgp'].value_counts() / df_students_selected.shape[0]

64.2% dos alunos tem media entre 6 e 8

df_students_selected['n_semestre_entrada'].value_counts() / df_students_selected.shape[0]
76.7% dos alunos entraram no período 1
'''

percent = (df_students_selected['estado'].value_counts() / 
    df_students_selected.shape[0])['irregular']
# Apriori
'''
confiança em torno de 60, suporte
'''
values = df_students_selected.astype(str).values
rules = fim.fpgrowth(values,target='r',supp= 10,conf=60,report='(SXYCl')
del values, percent
#rules = fim.apriori(df,target='r',supp=10,conf=60,report='(SXYCl')
#rules = fim.eclat(df,target='r',supp=10,conf=60,report='(SXYCl')
df_results = pd.DataFrame(rules,columns=['consequent','antecedent','metrics'])
del rules
df_results =df_results[['antecedent','consequent','metrics']]
df_results[['item set support','body set support','head support','confidence','lift']] = pd.DataFrame(np.array(list(df_results['metrics'].values)))
del df_results['metrics']
df_results['rule'] = df_results['antecedent'].astype(str).str.replace("'","") + " => " + df_results['consequent'].astype(str).str.replace("'","")

# prunning
df_rules = df_results[df_results['consequent'].isin(df_students_selected['estado'].unique())]
del df_results, df_students_selected

df_rules['antecedent'] = df_rules['antecedent'].apply(list)

'''
remover regras com 'Masculino' no antecedent
remover regras com 'Ciência da Computação' no antecedent
remover regras com 'período 1' no antecedent
remover regras com 'escola particular ou pública' no antecedent
remover regras com 'escola pública' e 'com cota' no antecedent
remover regras com 'estadia <= 4 e 2010.1 < entrada <= 2013.1' no antecedent e 'irregular' no consequent

'''
df_rules = df_rules[~df_rules['antecedent'].apply(lambda x: 'Masculino' in x)]
df_rules = df_rules[~df_rules['antecedent'].apply(lambda x: 'Ciência da Computação' in x)]
df_rules = df_rules[~df_rules['antecedent'].apply(lambda x: 'período 1' in x)]
df_rules = df_rules[~df_rules['antecedent'].apply(lambda x: 'escola particular ou pública' in x)]
df_rules = df_rules[~df_rules['antecedent'].apply(lambda x: 'escola pública' in x and 'com cota' in x)]
df_rules = df_rules[~(df_rules['antecedent'].apply(lambda x: 'estadia <= 4 e 2010.1 < entrada <= 2013.1' in x) &
    df_rules['consequent'].apply(lambda x: 'irregular' == x))]

'''
removendo regras com frequencia baixa
frequencia baseada na porcentagem de alunos irregulares
'''
percent = 0.20
df_rules = df_rules[df_rules['item set support']/df_rules['head support'] >= percent]
del percent
'''
As regras encontradas indicam que o rumo do aluno é decidido nos primeiros semestres dos cursos.



O padrão associado ao início da estadia do aluno nos cursos foi: 
1: ['média <= 5']  	irregular , ou seja: são alunos que nunca obtiveram uma média positiva e, por isso, apresentam este problema desde o início do curso

Por outro lado, os padrões identificados nos alunos regulares mostram este grupo de alunos possui características como:
média mais alta, alunos jovens, entrada recente e escola pública
A presença ou ausência de cota não parece ter associação com a regularidade, pois está presente em ambas as formas nas regras
['estadia <= 4 e entrada > 2013.1', 'sem cota']	regular
['estadia <= 4 e entrada > 2013.1', 'com cota']	regular
['nascimento > 1992', 'sem cota'] regular
['nascimento > 1992', 'com cota'] regular
Regras como
 ['estadia <= 4 e entrada > 2013.1']	regular
['nascimento > 1992'] regular
destacam as variáveis que regulam essa associação, entrada recente e idade
'''

# prints
print("Número de regras encontradas:",len(df_rules))
df_rules.to_csv(results_path+'q2-fp_growth-results.csv',sep=";",index=False,encoding="latin-1")
